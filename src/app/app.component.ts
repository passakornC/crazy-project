import {Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {} from 'googlemaps';
import {TestServiceService} from './test-service.service';
import {$} from 'protractor';
import {ModalDirective} from 'ng-uikit-pro-standard';
import {TaxiService} from './taxi.service';
import {HttpHeaders} from '@angular/common/http';


@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'Crazy Project';
    private mapProp;
    private reason: String = '';
    private licensePlateNumber: String = '';
    private taxiDetail: String = '';

    @ViewChild('frame') gframeElement: ModalDirective;
    @ViewChild('gmap') gmapElement: ElementRef;
    mymap: google.maps.Map;
    private infoWindow = new google.maps.InfoWindow({
        maxWidth: 200
    });ก

    constructor(private testservice: TestServiceService, private taxiService: TaxiService) {
        this.mapProp = {
            center: new google.maps.LatLng(13.90448473, 100.5284493),
            zoom: 15,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

    }

    getAllTaxiDetail() {
        this.taxiService.getAllTaxiDetail().subscribe(data => {
                for (let item of data) {
                    // console.log(item);
                    this.createMarker(item['latitude'], item['longitude'], item['reason']);
                }
            }
        );
    }

    ngOnInit() {
        // this.taxiService.printSomething('Hi this is my service');
        // this.getAllTaxiDetail();

        this.mymap = new google.maps.Map(this.gmapElement.nativeElement, this.mapProp);

        this.getAllTaxiDetail();

        // for (let item of dataList) {
        //   this.createMarker(item['latitude'], item['longitude'], item['reason']);
        // }
    }

    addLocationToMap() {
        let self = this;
        this.gframeElement.hide();

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function (pos) {
                let mylat: number = pos.coords.latitude;
                let mylong: number = pos.coords.longitude;


                console.log('lat: ', mylat, 'long: ', mylong);
                // divElm.innerHTML = '<h1>Lat: '  + pos.coords.latitude.toString() + ', Long: ' + pos.coords.longitude.toString() + '</h1>';

                let mycenter = new google.maps.LatLng(mylat, mylong);
                // marker
                const contentString = '<div id="content"><p>License Plate Number: ' + self.licensePlateNumber + '</p><p>Reason: ' + self.reason + '</p><p>Taxi Detail: ' + self.taxiDetail + '</p></div>';
                const infoWindow = new google.maps.InfoWindow({
                    content: contentString
                });
                const marker = new google.maps.Marker({
                    position: new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude),
                    map: self.mymap,
                    title: 'Somewhere ...'
                });
                self.mymap.panTo(mycenter);
                marker.addListener('click', () => infoWindow.open(self.mymap, marker));
            });


        } else { // Not supported
            alert('Oops! This browser does not support HTML Geolocation.');
        }
    }

    private createMarker(latitude: string, longitude: string, reason: string) {
        let lat: number = parseFloat(latitude);
        let lng: number = parseFloat(longitude);
        // let contentString = `<div id="content"><h3>${reason}</h3></div>`;
        // let infoWindow = new google.maps.InfoWindow({
        //     content: contentString
        // });

        // marker
        const marker = new google.maps.Marker({
            // position: new google.maps.LatLng(13.765819, 100.569695),
            position: new google.maps.LatLng(lat, lng),
            map: this.mymap,
            title: reason
        });

        google.maps.event.addListener(marker, 'click', () => {
            let contentString = `<div id="content"><h4>${reason}</h4></div>`;
            this.infoWindow.setContent(contentString);
            this.infoWindow.open(this.mymap, marker);
        });
        // marker.addListener('click', () => {
        //     infoWindow.open(this.mymap, marker);
        // });
    }


}
