import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TaxiService {
  private taxiUrl = 'http://localhost:8090/crap/api/taxi';

  constructor(private http: HttpClient) {
  }

  getAllTaxiDetail(): Observable<any> {
    return this.http.get(this.taxiUrl);
  }

  printSomething(text: string) {
    console.log(text);
  }
}
